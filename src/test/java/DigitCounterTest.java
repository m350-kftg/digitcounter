import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DigitCounterTest {

    @Test
    public void noDigitInNumber() {
        // given
        DigitCounter counter = new DigitCounter(123456);
        // when
        int nrOfDigits = counter.countDigit(7);
        //then
        assertEquals(0, nrOfDigits);
    }

    @Test
    public void containsDigitThreeTimes() {
        // given
        DigitCounter counter = new DigitCounter(-712374567);
        // when
        int nrOfDigits = counter.countDigit(7);
        //then
        assertEquals(3, nrOfDigits);
    }

    @Test
    public void doesNotChangeMemberWhenCountingDigits() {
        // given
        DigitCounter counter = new DigitCounter(-712374567);
        // when
        int nrOfDigits1stCall = counter.countDigit(7);
        int nrOfDigits2ndCall = counter.countDigit(7);

        //then
        assertEquals(nrOfDigits1stCall, nrOfDigits2ndCall);
    }

    @Test
    public void failToCountNonDigit() {
        // given
        DigitCounter counter = new DigitCounter(712374567);
        // when
        int nrOfDigits = counter.countDigit(10);
        //then
        assertEquals(-1, nrOfDigits);
    }

    @Test
    public void failToCountNegativeDigit() {
        // given
        DigitCounter counter = new DigitCounter(712374567);
        // when
        int nrOfDigits = counter.countDigit(-1);
        //then
        assertEquals(-1, nrOfDigits);
    }

}
